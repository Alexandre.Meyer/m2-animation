# Master 2 ID3D - Animation, Corps Articulés Et Moteurs Physiques (3 ECTS)

Responsables de l'enseignement : [Alexandre Meyer](	
http://liris.cnrs.fr/alexandre.meyer), [Nicolas Pronost](	
http://liris.cnrs.fr/nicolas.pronost) et [Florence Zara](	
http://liris.cnrs.fr/florence.zara) - LIRIS, Université Lyon 1

Volume horaire : 13h30 CM, 16h30 TP

<img src="images/simu.png" width="400" class="center">

## Objectif de l'UE
<p style="text-align:justify;">Animation, Corps Articulés Et Moteurs Physiques (ACAMP) est une UE du tronc commun du Master 2e année ID3D de l'Université Lyon 1. Les cours ont lieu au à l'automne. L'objectif de l'UE est de donner les bases de l'animation en synthèse d'images. Nous aborderons les deux grandes familles de méthodes. L'animation basée sur des données, par exemple pour l'animation d'humain virtuel (données issues de capture de mouvement). Et l'animation basée sur un modèle physique pour la simulation de phénomènes naturels comme le mouvement de textiles ou de fluide. L'UE laissera une grande part à l'application pratique avec la réalisation de TPs en C++/OpenGL et avec Unity proposant d'animer par exemple des humains virtuels, des vêtements, des cordes, une surface d'eau, etc.</p>

## Thématiques abordées

### Animation de personnage (A. Meyer) - 6h CM, 6h45 TP
  * Cinématique directe et inverse
  * Skinning
  * Graphe d'animation et motion matching
  * Apprentissage machine et animation
  * [La page web de cette partie](personnage)

### Animation par modèles physiques (F. Zara) - 6h CM, 6h45 TP
  * Dynamique Newtonienne
  * Animaton d'objets déformables
  * Dynamique des objets rigides
  * Simulation de fluide
  * Collisions
  * [La page web de cette partie](https://perso.liris.cnrs.fr/florence.zara/Web/M2Animation.html)

### Contrôle de mouvement (Nicolas Pronost) - 1h30 CM, 3h TP
  * Contrôle du mouvement d'objets rigides articulés
  * [La page web de cette partie](controle)


## Emploi du temps 2024-2025

![doc/Emploi-du-temps.png](doc/Emploi-du-temps.png)

  * Cours en salle TD10 du bâtiment Nautibus

  * TP en salles TP11, TP12 du bâtiment Nautibus

## Modalités de contrôle des connaissances (MCC)
   * **1 note de CCF** portant sur les 3 parties du cours
   * **3 notes de TP** : TP F. Zara, TP A. Meyer, TP N. Pronost (code + rapport + démo ou vidéo)

   * **Dates des évaluations** : 
     * Examen écrit : vendredi 26 janvier 2024, 13h30-15h30 en salle ?? du bâtiment Nautibus
     * Démo de TP : vendredi 26 janvier 2024, à partir de 15h45 en salles TP11 et TP12 du bâtiment Nautibus
     * Date limite de rendu des 3 archives : vendredi 26 janvier 2024 à 16h

  * **Modalité de rendu des TPs :** <p style="text-align:justify;">
     * Une archive sera à déposer sur TOMUSS (dans les 3 colonnes correspondantes). Cette archive contiendra le code du TP + un rapport.
     * Nous vous demandons également de mettre dans les 3 autres colonnes correspondantes de TOMUSS, l'URL pour accéder à une vidéo de votre projet, si vous ne faites pas de démo (car nous ne le compilerons pas forcément, donc il faut montrer tout votre travail).</p>

