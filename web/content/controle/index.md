# Master 2 ID3D - Animation, Corps Articulés Et Moteurs Physiques (3 ECTS)

## Contrôle de mouvement (Nicolas Pronost)

Télécharger [les transparents du cours](../doc/doc_controle/M2ANIM_CM_CONTROLEUR.pdf)


Télécharger [l'énoncé du TP](../doc/doc_controle/M2ANIM_TP_CONTROLEUR.pdf)


Accéder [aux ressources pour le TP](../doc/doc_controle/M2ANIM_TP_CONTROLEUR.zip)
