---
title: "Animation de personnage"
---


<img src="doc/chara.jpg" width="400" class="center">


## Les Cours
  
#### (CM1) Systèmes articulés, cinématique directe et Quaternions

* [Cinématique Directe](doc/M1_1_SkeletonBasedAnimation.pdf)
* [Quaternion](doc/M2_1b_Quaternions.pdf)

Il y a des parties en commun avec le M1 mais il s'agit de la base ! En nouveauté : les quaternions et la prise en main d'Unity. Regardez également

* [cette vidéo interactive expliquants les quaternions;](https://eater.net/quaternions/video/intro)
* [ce web interactif de manipulation des angles d'Euler.](http://www.ctralie.com/Teaching/COMPSCI290/Materials/EulerAnglesViz/)


#### (CM2) Cinématique inverse et Animations procédurales

* IK : méthode géométrique, descente de gradient, optimisation avec le Jacobien, heuristique (CCD, FABRIK)
* Animations procédurales

[Le PDF du cours est ici.](doc/M2_2_CinematiqueInverseIK_ProceduralAnimation.pdf)
  
#### (CM3) Edition d'animations et contrôle d'un personnage (sans physique)

* Motion graphe
* Machine à état + blend (Unity)
* Motion matching

[Le PDF du cours est ici.](doc/M2_3_MotionControlAndEditing.pdf)
  
#### (CM4) Skinning
[Le PDF du cours est ici.](doc/M2_4_Skinning.pdf)

#### (CM5) Deep learning et animation
[Le PDF du cours est ici.](doc/M2_5_DeepLearning_Animations.pdf)



<img src="doc/chara_ik.jpg" width="400" class="center">

## TP Animation de personnage

* [TP1.a les bases d'Unity](doc/M2_TP1a_AnimPersonnageUnity_LesBases.pdf)
* [TP1.b MecANim](doc/M2_TP1b_AnimPersonnageUnity-MecAnim.pdf)
* [TP2 Cinématique inverse avec FABRIK](doc/M2_TP2_AnimPersonnageUnity-IK.pdf) (la partie importante du TP).
    * Les 3 classes vides présentées dans le PDF
    * [IK.cs](doc/M2_TP2_AnimPersonnageUnity-IK_classIK.cs)
    * [IKJoint.cs](doc/M2_TP2_AnimPersonnageUnity-IK_classIKJoint.cs)
    * [IKChain.cs](doc/M2_TP2_AnimPersonnageUnity-IK_classIKChain.cs)

### Notation

* Pas besoin de faire de rapport pour cette partie
* Les archives sont à rendre sur TOMUSS. 
* [Le barème est ici.](https://docs.google.com/spreadsheets/u/0/d/10dxPOl4PPYM4w064hCYTlV5PhX5Sfo58w3TuuYE_VVo/pub?single=true&gid=0&range=A1:E21&output=html)

### Anciens TP

* [Edition multi-résolution d'une animation](doc/M2_TP8_AnimPersonnageUnity-MultiresEditing.pdf)
* [Edition d'animation (C++)](doc/M2_TP9_AnimPersonnageCPP.pdf)
